<?php
	
	/*

		CREDIT
		---------------------------------------
		Author: 	Martin Hlavacka
		Contact: 	martinhlavacka@outlook.com 
		Date: 		06.04.2018
				
		LICENSE
		---------------------------------------
		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.

		USAGE
		---------------------------------------
		1. Register on Gatewayapi and create API token
		2. Top-up your account
		3. Update prepared values

	*/

	// PREPARE VALUES
	$sms_sender 	= "NAME-OF-SENDER";
	$sms_api 		= "API-TOKEN-FROM-GATEWAYAPI";
	$sms_recepients = [FIRST-RECIPIENT, SECOND-RECIPIENT]; // IN FORMAT 4571000000
	$sms_message 	= "MESSAGE-TO-SEND";

	// CREATE NEW OBJECT
	$smsGateway = new SMSGateway($sms_sender, $sms_api, $sms_recepients ,$sms_message);

	// DISPLAY RESULT
	echo "<pre>";
		print_r($smsGateway);
	echo "</pre>";

	// CLASS DEFINITION
	class SMSGateway{

		// PROPERTIES
		private $url;
		private $sender;
		private $api_token;
		private $recipients;
		private $message;
		
		// CONSTRUCTOR
		public function __construct($sender, $api, $recipients, $message){

			// ASSIGN VALUES
			$this->sender 		= $sender;
			$this->url 			= "https://gatewayapi.com/rest/mtsms";
			$this->api_token 	= $api;
			$this->recipients 	= $recipients;
			$this->message 		= $message;

			// PREPARE DATA
			$json = [
				'sender' 		=> $this->sender,
				'message' 		=> $this->message,
				'recipients' 	=> []
			];

			// RECEPIENTS
			foreach ($recipients as $msisdn) {
				$json['recipients'][] = ['msisdn' => $msisdn];
			}

			// API CALL
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL, $this->url);
			curl_setopt($ch,CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
			curl_setopt($ch,CURLOPT_USERPWD, $this->api_token . ":");
			curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($json));
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

			// EXECUTE
			$result = curl_exec($ch);

			// CLOSING API CALL
			curl_close($ch);

			// RETURN RESULT
			return $result;

		}

	}

?>